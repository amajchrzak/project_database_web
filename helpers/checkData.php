<?php

require_once("../src/Utils/debug.php");

isset($_GET['formData']) ? $formData = $_GET['formData'] : "";
isset($_GET['data']) ? $data = $_GET['data'] : "";

if ($data == 0) {
    $query = "SELECT login FROM users";
    connectPDO('login', $query, $formData, 'Login');
} else {
    $query = "SELECT e_mail FROM users";
    connectPDO('e_mail', $query, $formData, 'Email');
}

function specialMark(string $tab): string
{
    $newString = "";
    for ($i = 0; $i < strlen($tab); $i++) {
        $newString .= $tab[$i];
        if ($tab[$i] == '@') {
            continue;
        }
    }
    return $newString;
}

function connectPDO($getter, $query, $form, $inputForm)
{
    $servername = 'localhost';
    $username = 'foodtruck_u';
    $password = 'foodtruckhaslo';
    $dbname = 'foodtruck';
    try {
        $dsn = "mysql:dbname={$dbname};host={$servername}";
        $conn = new PDO(
            $dsn,
            $username,
            $password,
            [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
        );
        $stmt = $conn->prepare($query);
        $stmt->execute();
        while ($row = $stmt->fetch()) {
            if ($form == $row[$getter]) {
                echo "Ten " . $inputForm . " jest zajęty";
                continue;
            }
        }
    } catch (PDOException $e) {
        echo "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
}