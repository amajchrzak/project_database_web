var XMLHttpRequestObject = false;
var countButtonclick = 2;
if (window.XMLHttpRequest) {
    XMLHttpRequestObject = new XMLHttpRequest();
} else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
}

function showMoreComments(dest, id, object) {
    if (XMLHttpRequestObject) {
        var commentCount = 2 * countButtonclick;
        var divCommentaries = document.getElementById(dest);
        var url = "helpers/showMoreComments.php?comments=" + commentCount + "&user=" + id + "&object=" + object;
        XMLHttpRequestObject.open('GET', url);
        XMLHttpRequestObject.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                countButtonclick++;
                divCommentaries.innerHTML = this.responseText;
            }
        }
    }
    XMLHttpRequestObject.send(null);
}

function showAllComments(dest, id, rank, object) {
    if (XMLHttpRequestObject) {
        var divCommentaries = document.getElementById(dest);
        var url = "helpers/showMoreComments.php?user=" + id + "&rank=" + rank + "&object=" + object;
        XMLHttpRequestObject.open('GET', url);
        XMLHttpRequestObject.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                divCommentaries.innerHTML = this.responseText;
                document.getElementById('btSubmit').disabled = true;
            }
        }
    }
    XMLHttpRequestObject.send(null);
}

function hideComments(dest, id, object) {
    if (XMLHttpRequestObject) {
        var commentCount = 2;
        var divCommentaries = document.getElementById(dest);
        var url = "helpers/showMoreComments.php?comments=" + commentCount + "&user=" + id + "&object=" + object;
        XMLHttpRequestObject.open('GET', url);
        XMLHttpRequestObject.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                divCommentaries.innerHTML = this.responseText;
                document.getElementById('btSubmit').disabled = false;
                countButtonclick = 2;
            }
        }
    }
    XMLHttpRequestObject.send(null);
}
