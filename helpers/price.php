<?php
declare(strict_types=1);
require_once("../src/Utils/debug.php");

if (isset($_GET['amount'])) {
    $amount = $_GET['amount'];
}
if (isset($_GET['price'])) {
    $price = $_GET['price'];
}
$sum = 0;
$products = [];
$prices = [];


$products = stringToArray($amount, $products);
$prices = stringToArray($price, $prices);
$sum = countSum($products, $prices);

echo $sum . "zł";

//convert string to array to get array of data
function stringToArray(string $temp, array $expected)
{
    $j = 0;
    for ($i = 0; $i < strlen($temp); $i++) {
        if ($temp[$i] != ',') {
            if (!isset($expected[$j])) {
                $expected[$j] = null;
            }
            $expected[$j] .= $temp[$i];
        } else {
            $j++;
        }
    }
    return $expected;
}

//count sum of products
function countSum(array $products, array $prices)
{
    $sum = 0;

    for ($i = 0; $i < count($products); $i++) {
        $sum += $products[$i] * $prices[$i];
    }

    return $sum;
}