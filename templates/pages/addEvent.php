<?php $trucks = $params['trucks']; ?>
<?php $valid = true; ?>

<div class="add-food-truck">
    <input type="text" id="name" name="nameEvent" placeholder="Nazwa" form="addEvent" onkeyup="valid('name', 20)"/>
    <div id="iname" class="formError"></div>
    <input type="text" id="city" name="cityEvent" placeholder="Miasto" form="addEvent" onkeyup="valid('city', 20)"/>
    <div id="icity" class="formError"></div>
    <textarea id="description" name="description" placeholder="Opis" form="addEvent"
              onkeyup="valid('description', 50)"> </textarea>
    <div id="idescription" class="formError"></div>
    <label for="created">Start date:</label>
    <input type="date" id="created" name="created" value="2021-01-15" min="2021-01-01" max="2021-12-31"
           form="addEvent"/>
</div>

<div style="clear:both;"></div>

<h3>Lista food trucków</h3>
<div class="tbl-header-2">
    <table cellpadding="0" cellspacing="0" border="0">
        <thead>
        <tr class="addEvent">
            <th>Nazwa</th>
            <th>Dostępność</th>
        </tr>
        </thead>
    </table>
</div>
<div class="tbl-content-2">
    <table cellpadding="0" cellspacing="0" border="0">
        <tbody>
        <?php foreach ($trucks as $truck): ?>
            <tr class="addEvent">
                <td><?php echo $truck['name']; ?></td>
                <td>
                    <select id="rank" name="availability_id=<?php echo $truck['id']; ?>[]" form="addEvent">
                        <option value="0">0</option>
                        <option value="1">1</option>
                    </select>
                </td>
                <input type="hidden" id="truckId" name="truckId_id=<?php echo $truck['id'] ?>[]"
                       value="<?php echo $truck['id'] ?>" form="addEvent"/>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<form action="/?action=addEvent" method="POST" id="addEvent">
    <input type="submit" name="addEvent" value="Dodaj Wydarzenie"/>
</form>

<?php
function validEvent($value, $length)
{
    if (strlen($value) < 0) {
        $valid = false;
    } else {
        if (strlen($value) > $length) {
            $valid = false;
        }
    }

    if ($valid != true) {
        echo "<div class='errorRegister'>Wprowadzono błędne dane, nie udało się utworzyć wydarzenia.</div>";
    }
}

?>