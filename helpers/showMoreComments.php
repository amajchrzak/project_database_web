<?php
require_once("../src/Utils/debug.php");

$servername = 'localhost';
$username = 'foodtruck_u';
$password = 'foodtruckhaslo';
$dbname = 'foodtruck';


if (isset($_GET['comments'])) {
    $commentNewCount = $_GET['comments'];
}
$id = $_GET['user'];
$object = $_GET['object'];
if ($object == 0) {
    if (!isset($_GET['rank'])) {
        $query = "
                    SELECT notes.id_event, users.login, notes.id_note, notes.title, notes.description, notes.date
                    FROM notes, users, events
                    WHERE notes.id_user = :id
                    AND users.id_user = notes.id_user
                    AND events.id_event = notes.id_event
                    AND notes.login = users.login
                    LIMIT :commentNewCount
                ";
        $rows = connectPDO($query, $commentNewCount, $id);
    } else {
        $query = "
                    SELECT notes.id_event, users.login, notes.id_note, notes.title, notes.description, notes.date
                    FROM notes, users, events
                    WHERE notes.id_user = :id
                    AND users.id_user = notes.id_user
                    AND events.id_event = notes.id_event
                    AND notes.login = users.login
                ";
        $rows = connectPDO($query, null, $id);
    }
} else {
    if (!isset($_GET['rank'])) {
        $query = "
                SELECT notes.id_note, users.login, notes.description, notes.date, notes.title
                FROM users, notes, events
                WHERE users.login = notes.login 
                AND notes.id_event = :id
                AND users.id_user = notes.id_user
                AND notes.id_event = events.id_event
                AND notes.login = users.login
                LIMIT :commentNewCount
            ";
        $rows = connectPDO($query, $commentNewCount, $id);

    } else {
        $query = "
                    SELECT notes.id_note, users.login, notes.description, notes.date, notes.title
                    FROM users, notes, events
                    WHERE users.login = notes.login 
                    AND notes.id_event = :id
                    AND users.id_user = notes.id_user
                    AND notes.id_event = events.id_event
                    AND notes.login = users.login
            ";
        $rows = connectPDO($query, null, $id);
    }
}
//dump($rows);
if ($rows > 0) {
    foreach ($rows as $row) {
        ?>
        <table cellpadding="0" cellspacing="0" border="0">
            <tbody>
            <tr>
                <div id="comments">
                    <td><?php echo $row['id_note'] ?></td>
                    <td><?php echo $row['login'] ?>
                    <td><?php echo $row['title'] ?></td>
                    <td><?php $row['description'] = substr($row['description'], 0, 20) . "...";
                        echo $row['description'] ?></td>
                    <td><?php echo $row['date'] ?></td>
                    <td>
                        <a href="/?action=showNote&id=<?php echo $row['id_event']; ?>&id_note=<?php echo $row['id_note'] ?>">
                            <button class="eventButton" type="submit" value="click">Szczegóły</button>
                        </a>
                        <a href="/?action=deleteNote&id_event=<?php echo $row['id_event']; ?>&id_note=<?php echo $row['id_note']; ?>">
                            <button class="eventButton" type="submit" value="click">USUN</button>
                        </a>
                    </td>
                </div>

            </tr>
            </tbody>
        </table>
        <?php
    }
} else {
    echo "there are no comments";
}

function connectPDO($query, ?int $commentNewCount, ?int $id)
{
    $servername = 'localhost';
    $username = 'foodtruck_u';
    $password = 'foodtruckhaslo';
    $dbname = 'foodtruck';

    try {
        $dsn = "mysql:dbname={$dbname};host={$servername}";
        $conn = new PDO(
            $dsn,
            $username,
            $password,
            [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
        );
        $stmt = $conn->prepare($query);
        if (isset($commentNewCount)) {
            $stmt->bindValue(':commentNewCount', $commentNewCount, PDO::PARAM_INT);
        }
        if (isset($id)) {
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        }

        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    } catch (PDOException $e) {
        echo "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
}

?>