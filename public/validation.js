function valid(elementId, lengthOfText) {
    var errorId = "i" + elementId;
    var element = document.getElementById(elementId);
    var errorBox = document.getElementById(errorId);

    element.addEventListener('input', (event) => {
        if (event.target.value.length > lengthOfText) {
            errorBox.innerText = "Maksymalnie " + lengthOfText + " znaków!";
        } else if (event.target.value.length < 1) {
            errorBox.innerText = "Nic nie wpisałeś";
        } else {
            errorBox.innerText = "";
        }
    })
}

// //walidacja wszystkich pól formularza
valid('name', 20);
//valid('password', 20);
// valid('repassword', 20);
valid('email', 40);
