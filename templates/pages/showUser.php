<?php
//lambda print user data in table
$printUser = function (?string $dataUser) {
    echo isset($dataUser) ? $dataUser : "Nie Ustawiono.";
}
?>


<div class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nazwa</th>
            <th>E-Mail</th>
            <th>Wiek</th>
            <th>Git</th>
            <th>LinkedIn</th>
            <th>Ranga</th>
            <th>Opcje</th>
        </tr>
        </thead>
    </table>
</div>
<div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
        <tbody>
        <?php $users = $params['user']; ?>
        <?php foreach ($users as $user): ?>
            <tr>
                <td><?php $printUser($user['id_user']); ?></td>
                <td><?php $printUser($user['login']); ?></td>
                <td><?php $printUser($user['e_mail']); ?></td>
                <td><?php $printUser($user['wiek']); ?></td>
                <td><?php $printUser($user['git']); ?></td>
                <td><?php $printUser($user['linkedIn']); ?></td>
                <td>
                    <?php if ($user['user_rank'] == 0): ?>
                        <select id="rank" name="user_rank" form="updateUserRank">
                            <option value="0" selected>0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    <?php elseif ($user['user_rank'] == 1): ?>
                        <select id="rank" name="user_rank" form="updateUserRank">
                            <option value="0">0</option>
                            <option value="1" selected>1</option>
                            <option value="2">2</option>
                        </select>
                    <?php elseif ($user['user_rank'] == 2): ?>
                        <select id="rank" name="user_rank" form="updateUserRank">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2" selected>2</option>
                        </select>

                    <?php endif; ?>

                </td>
                <td><a href="/?action=deleteUser&id_user=<?php echo $user['id_user']; ?>">
                        <button class="eventButton" type="submit" value="click">Usuń</button>
                    </a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<form method="post" action="/?action=updateUserRank&id_user=<?php echo $user['id_user']; ?>" id="updateUserRank">
    <div class="addEvent"><input type="submit" value="Zatwierdź"/></div>
</form>