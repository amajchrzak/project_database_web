<form action="/?action=addFoodTruck" method="post" enctype="multipart/form-data">
    <div>
        <label for="nameOfImage"></label>
        <div class="addTruckFormInput"><input type="text" id="nameOfImage" name="nameOfImage"
                                              placeholder="Nazwa zdjęcia" onkeyup="valid('nameOfImage', 20)"/>
            <div id="inameOfImage" class="formError"></div>
        </div>
    </div>
    <div>
        <label for="foodTruckImg"></label>
        <input type="file" name="foodTruckImg" value="foodTruckImg"/>
    </div>
    <div>
        <label for="nameFoodTruck"></label>
        <div class="addTruckFormInput"><input type="text" id="nameFoodTruck" name="nameFoodTruck"
                                              placeholder="Nazwa Food Trucka" onkeyup="valid('nameFoodTruck', 20)"/>
            <div id="inameFoodTruck" class="formError"></div>
        </div>
    </div>
    <div>
        <label for="descriptionTruck"></label>
        <div class="addTruckFormText"><textarea id="descriptionTruck" name="descriptionTruck" placeholder="Opis"
                                                onkeyup="valid('descriptionTruck', 300)"></textarea>
            <div id="idescriptionTruck" class="formError"></div>
        </div>
    </div>
    <div id="howManyProducts">
        <input type="text" id="howMany" name="howMany" placeholder="ile produktów ?"/>
        <input type="button" id="btSubmit" value="submit" onclick="howManyProducts('howMany');">
    </div>
    <input type="submit" name="submit" value="Zatwierdź"/>
</form>