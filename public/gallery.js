var XMLHttpRequestObject = false;
var countButtonclick = 2;
if (window.XMLHttpRequest) {
    XMLHttpRequestObject = new XMLHttpRequest();
} else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
}

function getImgDiv(imgId, destination) {
    var div = document.getElementById(destination);
    var url = "helpers/gallery.php?imgId=" + imgId;
    XMLHttpRequestObject.open("GET", url);
    XMLHttpRequestObject.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var str = this.responseText;
            div.innerHTML = str;
        }
    }
    XMLHttpRequestObject.send(null);
}