<?php
$events = $params['events'];
?>
<h3>Wydarzenia</h3>
<?php foreach ($events as $event): ?>
    <div class="event">
        <div class="eventText">
            <?php
            echo "Numer: " . "<strong>" . $event['id_event'] . "</strong>" . "<br/>";
            echo "Nazwa: " . "<strong>" . $event['nameEvent'] . "</strong>" . "<br/>";
            echo "Miejsce: " . "<strong>" . $event['cityEvent'] . "</strong>" . "<br/>";
            echo "Opis: " . "<strong>" . $event['description'] . "</strong>" . "<br/>";
            echo "Data: " . "<strong>" . $event['created'] . "</strong>" . "<br/>";
            ?>
        </div>
        <?php if (isset($_SESSION['id_user'])): ?>
            <a href="/?action=showEvent&id=<?php echo $event['id_event'] ?>">
                <button class="eventButton" type="submit"
                        value="click"><?php echo "Wydarzenie " . $event['id_event']; ?></button>
            </a>
        <?php endif; ?>
    </div>
<?php endforeach; ?>

<?php if (!isset($_SESSION['id_user'])): ?>
    <div class="require">
        <p> Aby zobaczyć szczegółowe informacje o danej imprezie należy się zalogować</p>
    </div>
<?php endif; ?>
<?php if (isset($_SESSION['id_user']) && $_SESSION['user_rank'] == 1): ?>
    <div class="addEvent">
        <a href="/?action=addEvent">
            <button class="eventButton" type="submit" value="click">Dodaj Wydarzenie</button>
        </a>
    </div>
    <div class="addEvent">
        <a href="/?action=getEvents">
            <button class="eventButton" type="submit" value="click">Edytuj Wydarzenia</button>
        </a>
    </div>
<?php endif; ?>
<!-- <div style="{clear:both;}"></div> -->
<div style="clear:both"></div>
<h3>Lista Food Trucków</h3>
<?php $images = $params['images'] ?>
<div class="foodTruckList">
    <?php foreach ($images as $image): ?>
        <a href="/?action=showFoodTruck&id=<?php echo $image['id'] ?>">
            <div class="imgFoodTruck">
                <?php echo '<img src="data:image/jpeg;base64,' . base64_encode($image['image']) . '"/>'; ?>
                <div class="desc"><?php echo $image['name']; ?></div>
            </div>
        </a>
    <?php endforeach; ?>
</div>
<?php if (isset($_SESSION['id_user']) && ($_SESSION['user_rank'] == 2 || $_SESSION['user_rank'] == 1)): ?>
    <div class="addEvent">
        <a href="/?action=addFoodTruck">
            <button class="eventButton" type="submit" value="click">Dodaj Food Trucka</button>
        </a>
    </div>
<?php endif; ?>
<div style="clear:both"></div>
<h3>Galeria Wydarzeń</h3>
<div id="gallery"></div>
