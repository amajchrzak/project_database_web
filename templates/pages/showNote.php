<?php
//lambda print user data in table
$printUser = function (?string $dataUser) {
    echo isset($dataUser) ? $dataUser : "Nie Ustawiono.";
}
?>

<?php $note = $params['note']; ?>
<?php foreach ($note as $info): ?>
    <div class="note">ID użytkownika, który komentarz utworzył: <strong><?php $printUser($info['id_user']); ?></strong>
    </div>
    <div class="note">ID komentarza: <strong><?php $printUser($info['id_note']); ?></strong></div>
    <div class="note">Login twórcy komentarza: <strong><?php $printUser($info['login']); ?></strong></div>
    <div class="note">Opis komentarza: <strong><?php $printUser($info['description']); ?></strong></div>
    <div class="note">Data utworzenia komentarza: <strong><?php $printUser($info['date']); ?></strong></div>
    <div class="note">ID wydarzenia, w którym istnieje komentarz:
        <strong><?php $printUser($info['id_event']); ?></strong></div>

    <?php if ($_SESSION['user_rank'] == 1): ?>
        <a href="/?action=deleteNote&id_event=<?php echo $info['id_event']; ?>&id_note=<?php echo $info['id_note']; ?>">
            <button class="eventButton" type="submit" value="click">USUN</button>
        </a>
    <?php endif; ?>
<?php endforeach; ?>