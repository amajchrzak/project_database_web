<div class="profile">
    <div class="profile_img">
        <?php $thread = 'resources/profile_img/'; ?>
        <?php $profileImageExist = false; ?>
        <?php foreach (scandir($thread) as $test): ?>
            <?php if ($_SESSION['id_user'] . '.jpg' === $test): ?>
                <?php $profileImageExist = true ?>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php if ($profileImageExist): ?>
            <div id="profileImg">
                <img class="profilePhoto" src="resources/profile_img/<?php echo $_SESSION['id_user'] ?>.jpg"/>
            </div>
        <?php else: ?>
            <img id="blank_profile" src="resources/blank_profile.jpg" style="width:240px;"/>

        <?php endif; ?>
    </div>
    <div>
        <div class="data">
            <?php if (isset($params['userData'][0]['login'])): ?>
                <span>Imie:</span>
                <?php echo $params['userData'][0]['login'] ?>
            <?php else: ?>
                <p>Imie: Nie dodano</p>
            <?php endif; ?>
        </div>

        <div class="data">
            <?php if (isset($params['userData'][0]['nazwisko'])): ?>
                <span>Nazwisko:</span>
                <?php echo $params['userData'][0]['nazwisko'] ?>
            <?php else: ?>
                <p>Nazwisko: Nie dodano</p>
            <?php endif; ?>
        </div>
        <div class="data">
            <?php if (isset($params['userData'][0]['wiek'])): ?>
                <span>Wiek:</span>
                <?php echo $params['userData'][0]['wiek'] ?>
            <?php else: ?>
                <p>Wiek: Nie dodano</p>
            <?php endif; ?>
        </div>
        <div class="data">
            <?php if (isset($params['userData'][0]['git'])): ?>
                <span>Git:</span>
                <?php echo $params['userData'][0]['git'] ?>
            <?php else: ?>
                <p>Git: Nie dodano</p>
            <?php endif; ?>
        </div>
        <div class="data">
            <?php if (isset($params['userData'][0]['linkedIn'])): ?>
                <span>LinkedIn:</span>
                <?php echo $params['userData'][0]['linkedIn'] ?>
            <?php else: ?>
                <p>LinkedIn: Nie dodano</p>
            <?php endif; ?>
        </div>

        <div>
            <a href="/?action=editProfile">
                <button class="editButton" type="submit" value="edytuj">Edytuj</button>
            </a>
        </div>
    </div>
    <h3>Moje Komentarze</h3>

    <div class="tbl-header-five">
        <table cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr>
                <strong>
                    <th>Id</th>
                </strong>
                <strong>
                    <th>Tytul</th>
                </strong>
                <strong>
                    <th>Opis</th>
                </strong>
                <strong>
                    <th>Data</th>
                </strong>
                <strong>
                    <th>Opcje</th>
                </strong>
            </tr>
            </thead>
        </table>
    </div>

    <div class="tbl-content-five">
        <div id="comments">
            <table cellpadding="0" cellspacing="0" border="0">
                <tbody>
                <?php foreach ($params['notes'] as $note): ?>
                <tr>
                    <!-- <div id="comments"> -->
                    <td><?php echo $note['id_note'] ?></td>
                    <td><?php echo $note['title'] ?></td>
                    <td><?php $note['description'] = substr($note['description'], 0, 20) . "...";
                        echo $note['description'] ?></td>
                    <td><?php echo $note['date'] ?></td>
                    <td>
                        <a href="/?action=showNote&id=<?php echo $note['id_event']; ?>&id_note=<?php echo $note['id_note'] ?>">
                            <button class="eventButton" type="submit" value="click">Szczegóły</button>
                        </a>
                        <a href="/?action=deleteNote&id_event=<?php echo $note['id_event']; ?>&id_note=<?php echo $note['id_note']; ?>">
                            <button class="eventButton" type="submit" value="click">USUN</button>
                        </a>
                    </td>
        </div>

        </tr>
        <?php endforeach; ?>
        </tbody>
        </table>
    </div>
</div>


</div>
<button class="notesButton" id="btSubmit" type="submit" value="click"
        onclick="showMoreComments('comments', <?php echo $_SESSION['id_user'] ?>, 0)">Więcej komentarzy
</button>
<button class="notesButton" id="btSubmit" type="submit" value="click"
        onclick="showAllComments('comments', <?php echo $_SESSION['id_user'] ?>, 1, 0)">Wszystkie komentarze
</button>
<button class="notesButton" id="btSubmit" type="submit" value="click"
        onclick="hideComments('comments', <?php echo $_SESSION['id_user'] ?>, 0)">Ukryj komentarze
</button>
</div>
<div style="clear:both;"></div>
<?php if (isset($params['trucks'])): ?>
    <?php $trucks = $params['trucks']; ?>
    <?php foreach ($trucks as $truck): ?>
        <h3 style="font-size:32px;"><?php echo $truck['nameImage'] ?></h3>
        <div class="truck">
            <div class="imgFoodTruck">
                <?php echo '<img src="data:image/jpeg;base64,' . base64_encode($truck['image']) . '"/>'; ?>
                <!-- <div class="desc"><?php //echo $truck['name'];?></div> -->

            </div>
            <div class="testx"><p id="descriptionTruck"><?php echo $truck['description'] ?></p></div>


        </div>
        <div style="clear:both;"></div>
        <div><a href="/?action=updateTruck&id=<?php echo $truck['id']; ?>">
                <button class="notesButton" id="btSubmit" type="submit" value="click">
                    Edytuj <?php echo $truck['nameImage'] ?></button>
            </a></div>
        <div style="clear:both;"></div>
    <?php endforeach; ?>
<?php endif; ?>