<?php $foodTruck = $params['foodTruck'] ?>
<?php foreach ($foodTruck as $truck): ?>
    <div class="foodTruck"><h3><?php echo $truck['name'] ?></h3></div>
    <div class="mainImgTruck">
        <?php echo '<img src="data:image/jpeg;base64,' . base64_encode($truck['image']) . '"/>'; ?>
        <div class="imgName"><p><?php echo $truck['nameImage'] ?></p></div>
    </div>
    <div class="foodTruck"><p><?php echo $truck['description'] ?></p></div>

<?php endforeach; ?>


<h3>Moje Komentarze</h3>

<div class="tbl-header-fourth">
    <table cellpadding="0" cellspacing="0" border="0">
        <thead>
        <tr>
            <strong>
                <th>Produkt</th>
            </strong>
            <strong>
                <th>Opis</th>
            </strong>
            <strong>
                <th>Cena</th>
            </strong>
            <strong>
                <th>Ilość</th>
            </strong>
        </tr>
        </thead>
    </table>
</div>

<div class="tbl-content-fourth">
    <div id="comments">
        <table cellpadding="0" cellspacing="0" border="0">
            <tbody>
            <?php $products = $params['products']; ?>
            <?php $howManyProducts = count($products); ?>
            <?php $i = 0; ?>
            <?php foreach ($products

            as $product): ?>
            <tr>
                <!-- <div id="comments"> -->
                <td><?php echo $product['product'] ?></td>
                <td><?php echo $product['description'] ?></td>
                <td><?php echo $product['price'] ?></td>
                <input type="hidden" name="price[]" id="price<?php echo $i ?>" value="<?php echo $product['price'] ?>">
                <td><input type="number" name="amount[]" id='amount<?php echo $i ?>' step="1" value="0"
                           onchange="countPrice('currentPrice', <?php echo $howManyProducts ?>)" min="0"></td>
    </div>
    </tr>
    <?php $i++; ?>
    <?php endforeach; ?>
    </tbody>
    </table>
</div>
</div>

<div class="currentPrice" id="currentPrice">0zł</div>
<div id="helpingString">Aktualna Cena:</div><
<div style="clear:both;"></div>
<div class="addEvent">
    <?php if (isset($_SESSION['id_user'])): ?>
        <a href="">
            <button class="eventButton" type="submit" value="click">Zapłać</button>
        </a>
    <?php endif; ?>
</div>
