<div class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
        <thead>
        <tr>
            <th>Numer</th>
            <th>Nazwa</th>
            <th>Miejsce</th>
            <th>Opis</th>
            <th>Data</th>
        </tr>
        </thead>
    </table>
</div>
<div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
        <tbody>
        <?php $events = $params['events'] ?>
        <?php foreach ($events as $event): ?>
            <tr>
                <td><?php echo $event['id_event'] ?></td>
                <td><?php echo $event['nameEvent'] ?></td>
                <td><?php echo $event['cityEvent'] ?></td>
                <td><?php $event['description'] = substr($event['description'], 0, 20) . "...";
                    echo $event['description'] ?></td>
                <td><?php echo $event['created'] ?></td>
                <td>
                    <a href="/?action=showEvent&id_event=<?php echo $event['id_event']; ?>">
                        <button class="eventButton" type="submit" value="click">Szczegóły</button>
                    </a>
                    <a href="/?action=deleteEvent&id_event=<?php echo $event['id_event']; ?>">
                        <button class="eventButton" type="submit" value="click">USUN</button>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>