<?php

declare(strict_types=1);

namespace App\Model;

use App\Exception\StorageException;
use PDO;
use Throwable;

class NoteModel extends AbstractModel
{
    //Note, user can create comment
    public function createNote(array $data): void
    {
        try {
            $id_user = $data['id_user'];
            $login = $data['login'];
            $title = $data['title'];
            $description = $data['description'];
            $created = date('Y-m-d H:i:s');
            $id_event = $data['id_event'];

            $query = $this->conn->prepare("
                INSERT INTO notes(id_user, login, title, description, date, id_event)
                VALUES(?, ?, ?, ?, ?, ?)
            ");

            $query->bindValue(1, $id_user, PDO::PARAM_INT);
            $query->bindValue(2, $login, PDO::PARAM_STR);
            $query->bindValue(3, $title, PDO::PARAM_STR);
            $query->bindValue(4, $description, PDO::PARAM_STR);
            $query->bindValue(5, $created, PDO::PARAM_STR);
            $query->bindValue(6, $id_event, PDO::PARAM_INT);

            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się utworzyć notatki", 400, $e);
        }
    }

    //Note, shows all notes
    public function getNotes(int $id): array
    {
        try {
            $query = $this->conn->prepare("
                SELECT notes.id_note, users.login, notes.description, notes.date, notes.title
                FROM users, notes, events
                WHERE users.login = notes.login AND notes.id_event = :id
                AND users.id_user = notes.id_user
                AND notes.id_event = events.id_event
            ");

            $query->bindValue(':id', $id, PDO::PARAM_INT);

            $query->execute();

            $notes = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($notes === false) {
                throw new StorageException('Lista notatek jest równa 0', 400);
            }
            return $notes;
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się pobrać notatek", 400, $e);
        }
    }

    //Note, shows specificaly note
    public function getNote(int $id): array
    {
        try {
            $query = $this->conn->prepare("
                SELECT notes.id_user, notes.id_note, notes.login, notes.description, notes.date, notes.title, notes.id_event
                FROM users, notes, events
                WHERE users.id_user = notes.id_user 
                AND users.login = notes.login
                AND notes.id_event = events.id_event
                AND notes.id_note = :id
            ");

            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();

            $note = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($note === false) {
                throw new StorageException('Notatka nie istnieje', 400);
            }
            return $note;
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się pobrać notatki", 400, $e);
        }
    }
}