<?php

declare(strict_types=1);

namespace App\Controller;

class UserController extends AbstractController
{
    //--------------------USER------------------

    //User, if user is loged in, can logout himself
    public function logoutAction(): void
    {
        unset($_SESSION['id_user']);
        unset($_SESSION['name']);
        unset($_SESSION['password']);
        unset($_SESSION['e_mail']);
        unset($_SESSION['user_rank']);
        session_unset();
        header('Location: /?action=login');
    }


    //User, prints user profile
    public function showProfileAction(): void
    {
        $id = (int)$_SESSION['id_user'];
        $this->view->render('showProfile', [
            'notes' => $this->userModel->getUserNotes(),
            'userData' => $this->userModel->getUsers($id),
            'trucks' => $this->foodTruckModel->getFoodTruckProfile($id)
        ]);
    }

    //User, able to edit his own profile
    public function editProfileAction(): void
    {
        $id = (int)$_SESSION['id_user'];

        if ($this->request->hasPost()) {
            if (isset($_FILES['profileImg']) ?? '') {
                $this->addFile();
            } else {
                echo "Nie udało się załadować pliku";
            }
            $_SESSION['name'] = $this->request->postParam("name");
            $_SESSION['surname'] = $this->request->postParam("surname");
            $_SESSION['age'] = $this->request->postParam("age");
            $_SESSION['git'] = $this->request->postParam("git");
            $_SESSION['linkedIn'] = $this->request->postParam("linkedIn");

            $this->userModel->updateUser();
            $this->view->render('showProfile', [
                'editedFiles' => $_FILES['profileImg'],
                'notes' => $this->userModel->getUserNotes(),
                'userData' => $this->userModel->getUsers($id)
            ]);
        }
        $this->view->render('editProfile', [
            'userData' => $this->userModel->getUsers($id)
        ]);
    }




    /*----------------------------Private Methods -------------------- */

    //User, adding profile Image PRIVATE 
    private function addFile(): void
    {
        $threadsDir = "./resources/profile_img";
        $_FILES['profileImg']['name'] = (string)$_SESSION['id_user'];
        move_uploaded_file($_FILES['profileImg']['tmp_name'],
            $threadsDir . "/" . $_FILES['profileImg']['name'] . ".jpg");
    }

}
