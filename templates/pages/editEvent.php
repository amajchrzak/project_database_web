<div class="tbl-header">
        <table cellpadding="0" cellspacing="0" border="0">
            <thead>
                <tr>
                    <th>Numer</th>
                    <th>Nazwa</th>
                    <th>Miejsce</th>
                    <th>Opis</th>
                    <th>Data</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="tbl-content">
        <table cellpadding="0" cellspacing="0" border="0">
            <tbody>
                <?php $events = $params['events']?>
                <?php foreach($events as $event): ?>
                    <tr>
                        <td><?php echo $event['id_event']?></td>
                        <td><?php echo $event['nameEvent']?></td>
                        <td><?php echo $event['cityEvent']?></td>
                        <td><?php echo $event['description'] ?></td>
                        <td><?php echo $event['created']?></td>
                        <td>
                            <td><button><a href="/?action=showEvent&id_event=<?php echo $user['id_event'];?>">Szczegóły</a></button></td>
                            <!-- <a href="/?action=deleteEvent"><button class="eventButton" type="submit" value="click">USUN</button></a> -->
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>