<?php

declare(strict_types=1);

namespace App\Model;

use App\Exception\StorageException;
use PDO;
use Throwable;

class AdminModel extends AbstractModel
{
    //--------------------NOTE------------------
    //admin, delete specificaly note 
    public function deleteNote(int $id): void
    {
        try {
            $query = $this->conn->prepare("
                DELETE FROM notes
                WHERE id_note = :id
            ");
            $query->bindParam(':id', $id, PDO::PARAM_INT);
            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się skasować notatki o id równym $id", 400, $e);
        }
    }
    //--------------------USER------------------
    //Admin, delete specificaly user
    public function deleteUserById(?int $id): void
    {
        try {
            $query = $this->conn->prepare("
                DELETE FROM users
                WHERE id_user = :id
            ");

            $query->bindParam(':id', $id, PDO::PARAM_INT);
            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się skasować użytkownika o id $id", 400, $e);
        }
    }

    //Admin, can change user rank (1 - admin) (0 - user)
    public function updateUserRankById(array $userData): void
    {
        try {
            $userRank = $userData['user_rank'];
            $userId = $userData['id'];

            $query = $this->conn->prepare("
                UPDATE users
                SET user_rank = :userRank
                WHERE id_user = :userId
            ");

            $query->bindValue(':userRank', $userRank, PDO::PARAM_INT);
            $query->bindValue(':userId', $userId, PDO::PARAM_INT);

            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się zaktualizować użytkownika o id $userId", 400, $e);
        }
    }

    //--------------------EVENT------------------
    //Admin, adding new event
    public function addEvent(?array $eventData): void
    {
        try {
            $nameEvent = $eventData['nameEvent'];
            $cityEvent = $eventData['cityEvent'];
            $description = $eventData['description'];
            $created = $eventData['created'];

            $query = $this->conn->prepare("
                INSERT INTO events(nameEvent, cityEvent, description, created)
                VALUES (?, ?, ?, ?)
            ");

            $query->bindValue(1, $nameEvent, PDO::PARAM_STR);
            $query->bindValue(2, $cityEvent, PDO::PARAM_STR);
            $query->bindValue(3, $description, PDO::PARAM_STR);
            $query->bindValue(4, $created, PDO::PARAM_STR);
            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się dodać wydarzenia", 400, $e);
        }
    }

    //Admin, shows data of events if parameter exist get data of one event
    public function getEventsData(?int $id): array
    {
        if (isset($id)) {
            try {
                $query = $this->conn->prepare("
                SELECT id_event, nameEvent, cityEvent, description, created
                    FROM events
                    WHERE id_event = :id
                ");

                $query->bindValue(':id', $id, PDO::PARAM_INT);
            } catch (Throwable $e) {
                throw new StorageException("Nie udało się pobrać informacji o wydarzeniu $id", 400, $e);
            }

        } else {
            try {
                $query = $this->conn->prepare("
                    SELECT id_event, nameEvent, cityEvent, description, created
                    FROM events
                ");
            } catch (Throwable $e) {
                throw new StorageException("Nie udało się pobrać informacji o wydarzeniach", 400, $e);
            }
        }
        if (isset($query)) {
            $query->execute();
            $eventsData = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($eventsData === false) {
                throw new StorageException('Lista wydarzeń jest równa 0', 400);
            }
            return $eventsData;
        }
    }

    //Admin, deletes specificaly event
    public function deleteEvent(int $id): void
    {
        try {
            $query = $this->conn->prepare("
                DELETE FROM events
                WHERE id_event = :id
            ");

            $query->bindValue(':id', $id, PDO::PARAM_INT);

            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się usunąć wydarzenia", 400, $e);
        }
    }

    //--------------------Truck availability------------------
    //Admin, set availability of trucks in the events
    public function insertTruckAvailability(array $data): void
    {
        try {
            $truckId = (int)$data['truckId'][0];
            $availability = (int)$data['availability'][0];
            $idEvent = (int)$data['id'];


            $query = $this->conn->prepare("
                INSERT INTO availability(id_car ,availability, id_event)
                VALUES(?, ?, ?)
            ");

            $query->bindValue(1, $truckId, PDO::PARAM_INT);
            $query->bindValue(2, $availability, PDO::PARAM_INT);
            $query->bindValue(3, $idEvent, PDO::PARAM_INT);
            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się dodać dostępności food trucków", 400, $e);
        }
    }

    //Admin, update availability of trucks in the events
    public function updateTruckAvailability(array $data): void
    {
        try {
            $truckId = (int)$data['truckId'][0];
            $availability = (int)$data['availability'][0];
            $idEvent = (int)$data['id'];


            $query = $this->conn->prepare("
                UPDATE availability
                SET availability = :availability
                WHERE id_car = :truckId
                AND id_event = :idEvent
            ");

            $query->bindValue(':availability', $availability, PDO::PARAM_INT);
            $query->bindValue(':truckId', $truckId, PDO::PARAM_INT);
            $query->bindValue(':truckId', $idEvent, PDO::PARAM_INT);

            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się zaktualizować dostępności food trucków", 400, $e);
        }
    }


    //friend function which returns me last record in database
    //using in availability
    public function lastEventId(): int
    {
        try {

            $query = $this->conn->prepare("
               SELECT id_event 
                FROM events 
                ORDER BY id_event DESC 
                LIMIT 1
            ");

            $query->execute();
            $eventId = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($eventId === false) {
                throw new StorageException('Nie udało się pobrać informacji o ostatnim wydarzeniu', 400);
            }
            return (int)$eventId[0]['id_event'];
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się pobrać informacji o ostatnim wydarzeniu", 400, $e);
        }
    }

    public function deleteFoodTruck(int $id): void
    {
        try {
            $query = $this->conn->prepare("
                DELETE FROM cars
                WHERE id = :id
            ");
            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się usunąć food trucka", 400, $e);
        }
    }

    public function deleteFoodTruckMenu(int $id): void
    {
        try {
            $query = $this->conn->prepare("
                DELETE FROM products
                WHERE id_truck = :id
            ");

            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się usunąć menu food trucka", 400, $e);
        }
    }

    public function deleteFoodTruckAvailability(int $id): void
    {
        try {
            $query = $this->conn->prepare("
            DELETE FROM availability
            WHERE id_car = :id
        ");

            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się usunąć dostępności food trucka food trucka", 400, $e);
        }
    }
}