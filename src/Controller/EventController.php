<?php

declare(strict_types=1);

namespace App\Controller;

class EventController extends AbstractController
{
    //Note, shows event - form to add comment, commentaries and trucks attributables to event
    public function showEventAction(): void
    {
        if ($this->request->getParam('id')) {
            $eventId = (int)$this->request->getParam('id');
        } else {
            $eventId = (int)$this->request->getParam('id_event');
        }
        $this->view->render('showEvent', [
            'notes' => $this->noteModel->getNotes($eventId),
            'event' => $this->adminModel->getEventsData($eventId),
            'trucks' => $this->foodTruckModel->getAvailableTrucks($eventId)
        ]);
    }

}