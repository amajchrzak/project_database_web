<?php

require_once("../src/Utils/debug.php");


$imagesDir = "../resources/imagesOfEvents";

if (isset($_GET['imgId'])) {
    $imgId = $_GET['imgId'];
} else {
    $imgId = 0;
}

$direction = scandir($imagesDir);
array_shift($direction);
array_shift($direction);


$countImages = count($direction);
$nameOfImage = $direction[$imgId];

$first = 0;
$last = $countImages - 1;
$next = $imgId + 1;
$previous = $imgId - 1;
if ($next > $last) {
    $next = $first;
}

if ($previous < $first) {
    $previous = $last;
}
?>

<img src="<?php echo "$imagesDir/$nameOfImage"; ?>"/>
<div id="options">
    <a onclick="getImgDiv(<?php echo $first; ?>, 'gallery')">Pierwszy</a>
    <a onclick="getImgDiv(<?php echo $previous; ?>, 'gallery')"><</a>
    <a onclick="getImgDiv(<?php echo $next; ?>, 'gallery')">></a>
    <a onclick="getImgDiv(<?php echo $last; ?>, 'gallery')">Ostatni</a>
</div>