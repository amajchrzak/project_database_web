<?php

declare(strict_types=1);

namespace App\Controller;

class AdminController extends AbstractController
{
    //--------------------NOTES------------------

    //Admin, delete specificaly note
    public function deleteNoteAction(): void
    {
        $idEvent = (int)$this->request->getParam('id_event');
        $idNoteToDelete = (int)$this->request->getParam('id_note');
        $this->adminModel->deleteNote($idNoteToDelete);
        header("location: /?action=showEvent&id_event=$idEvent");
    }

    //--------------------USER------------------
    //Admin, page which returns whole users
    public function showUsersListAction(): void
    {
        $this->view->render('showUsersList', [
            'users' => $this->adminModel->getUsers(null)
        ]);
    }

    //Admin, prints specificaly user
    public function showUserAction(): void
    {
        $data['id'] = (int)$this->request->getParam('id_user');
        $this->view->render('showUser', [
            'user' => $this->adminModel->getUsers($data['id'])
        ]);
    }

    //admin can delete user
    public function deleteUserAction(): void
    {
        $data['id'] = (int)$this->request->getParam('id_user');
        $this->adminModel->deleteUserById($data['id']);
        header('location: /?action=showUsersList');
    }

    //admin can change rank of user (rank = 0 - normal user / rank = 1 - admin)
    public function updateUserRankAction(): void
    {
        $data['user_rank'] = $this->request->postParam("user_rank");
        $data['id'] = (int)$this->request->getParam('id_user');
        $this->adminModel->updateUserRankById($data);
        header("location: /?action=showUser&id_user=" . $data['id'] . "");
    }


    //--------------------EVENT------------------
    //Admin, adding next event
    public function addEventAction(): void
    {
        if ($this->request->hasPost()) {
            $data['nameEvent'] = $this->request->postParam('nameEvent');
            $data['cityEvent'] = $this->request->postParam('cityEvent');
            $data['description'] = $this->request->postParam('description');
            $data['created'] = $this->request->postParam('created');
            $this->adminModel->addEvent($data);
            $trucks = $this->foodTruckModel->showFoodTrucks(null);
            foreach ($trucks as $truck) {
                $availabilityId = "availability_id=" . $truck['id'];
                $truckId = "truckId_id=" . $truck['id'];
                $dataAvailability['truckId'] = $this->request->postParam($truckId);
                $dataAvailability['availability'] = $this->request->postParam($availabilityId);
                $dataAvailability['id'] = $this->adminModel->lastEventId();
                $this->adminModel->insertTruckAvailability($dataAvailability);
            }
            header('location: /');
        }
        $this->view->render('addEvent', [
            'trucks' => $this->foodTruckModel->showFoodTrucks(null)
        ]);
    }

    //Admin, page with whole events, 
    //with buttons which can print specificaly event or delete event
    public function getEventsAction(): void
    {
        $this->view->render('getEvents', [
            'events' => $this->adminModel->getEventsData(null)
        ]);
    }

    //Admin, delete specifiy event
    public function deleteEventAction(): void
    {
        $data = (int)$this->request->getParam('id_event');
        $this->adminModel->deleteEvent($data);
        header('location: /?action=getEvents');
    }

    //Admin, 
    public function updateFoodTruckAvailabilityAction(): void
    {
        $trucks = $this->foodTruckModel->showFoodTrucks(null);
        foreach ($trucks as $truck) {
            $availabilityId = "availability_id=" . $truck['id'];
            $truckId = "truckId_id=" . $truck['id'];
            $data['truckId'] = $this->request->postParam($truckId);
            $data['availability'] = $this->request->postParam($availabilityId);
            $data['id'] = (int)$this->request->getParam('id');
            $this->adminModel->updateTruckAvailability($data);
        }
        header("Location: /?action=showEvent&id=" . $data['id'] . "");
    }

    public function getFoodTrucksListAction(): void
    {
        $this->view->render('showFoodTrucks', [
            'trucks' => $this->foodTruckModel->showFoodTrucks(null)
        ]);
    }

    public function deleteFoodTruckAction(): void
    {
        $id = (int)$this->request->getParam('id');
        $this->adminModel->deleteFoodTruck($id);
        $this->adminModel->deleteFoodTruckMenu($id);
        $this->adminModel->deleteFoodTruckAvailability($id);
        $this->view->render('showFoodTrucks', [
            'trucks' => $this->foodTruckModel->showFoodTrucks(null)
        ]);
    }
}