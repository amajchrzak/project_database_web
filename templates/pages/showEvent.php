<?php
//lambda print user data in table
$printUser = function (?string $dataUser) {
    echo isset($dataUser) ? $dataUser : "Nie Ustawiono.";
}
?>

<?php
$events = $params['event'];
foreach ($events as $event) {
    $id = $event['id_event'];
    $nameEvent = $event['nameEvent'];
    $cityEvent = $event['cityEvent'];
    $description = $event['description'];
    $created = $event['created'];
}
?>
<div class="eventInfo">Numer festiwalu <strong> <?php echo $id; ?></strong></div>
<div class="eventInfo">Nazwa <strong><?php echo $nameEvent; ?></strong></div>
<div class="eventInfo">Miejsce <strong><?php echo $cityEvent; ?></strong></div>
<div class="eventInfo">Opis <strong><?php echo $description; ?></strong></div>
<div class="eventInfo">Data <strong><?php echo $created; ?></strong></div>

<div>
    <h3>Dodawanie komentarzy</h3>
    <div class="formOfNote">
        <form action="/?action=createNote&id=<?php echo $events[0]['id_event']; ?>" method="post">
            <input type="text" name="title" id="title" placeholder="Tytuł" onkeyup="valid('title', 20)" required/>
            <div id="ititle" class="formError"></div>
            <br/><br/>
            <textarea id="description" name="description" rows="10" cols="40" placeholder="Treść"
                      onkeyup="valid('description', 500)" required></textarea>
            <div id="idescription" class="formError"></div>
            <div class="submitComment"><input type="submit" value="Submit"/></div>
        </form>
    </div>
    <h3>Komentarze</h3>
    <div class="tbl-header">
        <table cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr>
                <th>Id</th>
                <th>Autor</th>
                <th>Tytuł</th>
                <th>Opis</th>
                <th>Data</th>
                <th>Opcje</th>
            </tr>
            </thead>
        </table>
    </div>
    <div class="tbl-content">
        <div id="comments">
            <table cellpadding="0" cellspacing="0" border="0">
                <tbody>
                <?php $notes = $params['notes'] ?>
                <?php foreach ($notes as $note): ?>
                    <tr>
                        <td><?php echo $note['id_note'] ?></td>
                        <td><?php echo $note['login'] ?></td>
                        <td><?php echo $note['title'] ?></td>
                        <td><?php $note['description'] = substr($note['description'], 0, 20) . "...";
                            echo $note['description'] ?></td>
                        <td><?php echo $note['date'] ?></td>
                        <td>
                            <a href="/?action=showNote&id=<?php echo $events[0]['id_event']; ?>&id_note=<?php echo $note['id_note'] ?>">
                                <button class="eventButton" type="submit" value="click">Szczegóły</button>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<button class="notesButton" id="btSubmit" type="submit" value="click"
        onclick="showMoreComments('comments', <?php echo $events[0]['id_event']; ?>, 1)">Więcej komentarzy
</button>
<button class="notesButton" id="btSubmit" type="submit" value="click"
        onclick="showAllComments('comments', <?php echo $events[0]['id_event']; ?>, 1, 1)">Wszystkie komentarze
</button>
<button class="notesButton" id="btSubmit" type="submit" value="click"
        onclick="hideComments('comments', <?php echo $events[0]['id_event']; ?>, 1)">Ukryj komentarze
</button>
</div>
<div style="clear:both;"></div>
<h3>Food Trucki</h3>
<div class="tbl-header-five">
    <table cellpadding="0" cellspacing="0" border="0">
        <thead>
        <tr>
            <th>Zdjęcie</th>
            <th>Nazwa</th>
            <th>Typ Jedzenia</th>
            <th>Opis</th>
            <th>Opcje</th>
        </tr>
        </thead>
    </table>
</div>
<div class="tbl-content-five">
    <table cellpadding="0" cellspacing="0" border="0">
        <tbody>
        <?php $trucks = $params['trucks'] ?>

        <?php foreach ($trucks as $truck): ?>
            <tr>
                <td><?php echo '<img src="data:image/jpeg;base64,' . base64_encode($truck['image']) . '" width="150" hright="150"/>'; ?></td>
                <td><?php echo $truck['name'] ?></td>
                <td><?php echo $truck['nameImage'] ?></td>
                <td><?php $truck['description'] = substr($truck['description'], 0, 40) . "...";
                    echo $truck['description'] ?></td>
                <input type="hidden" id="truckId" name="truckId_id=<?php echo $truck['id'] ?>[]"
                       value="<?php echo $truck['id'] ?>" form="availability"/>
                <!-- <td><?php if ($_SESSION['user_rank'] == 1): ?>
                            <select id="rank" name="availability_id=<?php //echo $truck['id'];?>[]" form="availability"> 
                                <option value="0">0</option>
                                <option value="1">1</option>
                            </select>
                        <?php endif; ?>
                    </td>     -->
                <td>
                    <a href="/?action=showFoodTruck&id=<?php echo $truck['id']; ?>">
                        <button class="eventButton" type="submit" value="click">Szczgóły</button>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<!-- <?php if ($_SESSION['user_rank'] == 1): ?>
<form method="post" action="/?action=updateFoodTruckAvailability&id=<?php echo $event['id_event']; ?>" id="availability">
    <div class="addEvent"><input type="submit" value="Zatwierdź"/></div>
</form>
<?php endif; ?> -->