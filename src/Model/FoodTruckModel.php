<?php

declare(strict_types=1);

namespace App\Model;

use App\Exception\StorageException;
use PDO;
use Throwable;

class FoodTruckModel extends AbstractModel
{
    //FoodTruck, get whole available trucks on the event
    public function getAvailableTrucks(int $eventId): array
    {
        try {
            $query = $this->conn->prepare("
                SELECT 
                cars.id, cars.name, cars.nameImage, cars.description, cars.image,
                availability.availability, availability.id_event
                FROM cars, availability
                WHERE cars.id = availability.id_car
                AND availability.id_event = :eventId
                AND availability.availability = 1
            ");

            $query->bindValue(':eventId', $eventId, PDO::PARAM_INT);

            $query->execute();

            $trucks = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($trucks === false) {
                throw new StorageException('Lista wydarzeń jest równa 0', 400);
            }
            return $trucks;
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się pobrać informacji na temat dostępności food trucków", 400, $e);
        }
    }


    //Food Truck, user can add Food Truck
    public function addFoodTruck(array $photoData, array $fileData): void
    {
        try {
            $idUser = $_SESSION['id_user'];
            $nameFoodTruck = $fileData['nameFoodTruck'];
            $nameImage = $photoData['name'];
            $typeImage = $photoData['type'];
            $imageData = file_get_contents($photoData['tmp_name']);
            $description = $fileData['descriptionTruck'];

            $query = $this->conn->prepare("
                INSERT INTO cars(name, nameImage, typeImage, image, description, id_user)
                VALUES(?, ?, ?, ?, ?, ?)
            ");

            $query->bindValue(1, $nameFoodTruck, PDO::PARAM_STR);
            $query->bindValue(2, $nameImage, PDO::PARAM_STR);
            $query->bindValue(3, $typeImage, PDO::PARAM_STR);
            $query->bindParam(4, $imageData, PDO::PARAM_LOB);
            $query->bindValue(5, $description, PDO::PARAM_STR);
            $query->bindValue(6, $idUser, PDO::PARAM_INT);

            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się dodać food trucka", 400, $e);
        }
    }

    //Food Truck, owner of food truck can select and then add products
    public function addProductList(array $truckId, string $product, string $price, string $description): void
    {
        try {
            $truck = $truckId['id'];
            $query = $this->conn->prepare("
                INSERT INTO products(id_truck, product, description, price)
                VALUES(?, ?, ?, ?)
            ");

            $query->bindValue(1, $truck, PDO::PARAM_INT);
            $query->bindValue(2, $product, PDO::PARAM_STR);
            $query->bindValue(3, $description, PDO::PARAM_STR);
            $query->bindValue(4, $price, PDO::PARAM_STR);

            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się dodać menu do foodtrucka", 400, $e);
        }
    }

    //Food Truck, shows menu of truck
    public function showProductList(int $id): array
    {
        try {
            $query = $this->conn->prepare("
                SELECT id_truck, product, description, price 
                FROM products
                WHERE id_truck = :id
            ");

            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();

            $products = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($products === false) {
                throw new StorageException('Menu nie istnieje', 400);
            }
            return $products;
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się pobrać informacji o menu foodtrucka", 400, $e);
        }
    }

    public function showFoodTrucks(?int $id): array
    {
        try {
            if (isset($id)) {
                $query = $this->conn->prepare("
                    SELECT id, name, nameImage, image, typeImage, description
                    FROM cars 
                    WHERE id = :id
                ");

                $query->bindValue(':id', $id, PDO::PARAM_INT);
            } else {
                $query = $this->conn->prepare("
                    SELECT id, name, nameImage, image, typeImage, description
                    FROM cars 
                ");
            }

            $query->execute();
            $trucks = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($trucks === false) {
                throw new StorageException('Lista food trucków jest równa 0', 400);
            }
            return $trucks;
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się pobrać informacji o foodtruckach", 400, $e);
        }
    }

    public function getFoodTruckProfile(int $id): array
    {
        try {
            $query = $this->conn->prepare("
                SELECT id, name, nameImage, image, typeImage, description
                FROM cars 
                WHERE id_user = :id
            ");

            $query->bindValue(':id', $id, PDO::PARAM_INT);

            $query->execute();
            $trucks = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($trucks === false) {
                throw new StorageException('Lista food trucków jest równa 0', 400);
            }
            return $trucks;
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się pobrać informacji o foodtruckach", 400, $e);
        }
    }

    //Food Truck, helper which shows last truck in database
    public function selectLastTruck(): array
    {
        try {
            $query = $this->conn->prepare("
                SELECT id 
                FROM cars
                ORDER BY id DESC
                LIMIT 1
            ");

            $query->execute();
            $truckId = $query->fetch(PDO::FETCH_ASSOC);
            if ($truckId === false) {
                throw new StorageException('Food Truck nie istnieje', 400);
            }
            return $truckId;
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się pobrać informacji o ostatnim food trucku", 400, $e);
        }
    }

    public function updateTruck(array $photoData, array $fileData, int $idTruck): void
    {
        try {
            $nameFoodTruck = $fileData['nameFoodTruck'];
            $nameImage = $photoData['name'];
            $typeImage = $photoData['type'];
            $imageData = file_get_contents($photoData['tmp_name']);
            $description = $fileData['descriptionTruck'];

            $query = $this->conn->prepare("
                UPDATE cars 
                SET name = :name, nameImage = :nameImage, typeImage = :typeImage, image = :image, description = :description
                WHERE id = :id
            ");

            $query->bindValue(':name', $nameFoodTruck, PDO::PARAM_STR);
            $query->bindValue(':nameImage', $nameImage, PDO::PARAM_STR);
            $query->bindValue(':typeImage', $typeImage, PDO::PARAM_STR);
            $query->bindParam(':image', $imageData, PDO::PARAM_LOB);
            $query->bindValue(':description', $description, PDO::PARAM_STR);
            $query->bindValue(':id', $idTruck, PDO::PARAM_STR);

            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się zaktualizować foodtrucka", 400, $e);
        }
    }

    public function updateProductList(array $product, array $price, array $description, int $idTruck, int $i): void
    {
        try {
            $truck = $idTruck;
            $query = $this->conn->prepare("
                UPDATE products
                SET product = :product, description = :description, price = :price
                WHERE product = :product
            ");

            $query->bindValue(':id', $truck[$i], PDO::PARAM_INT);
            $query->bindValue(':product', $product[$i], PDO::PARAM_STR);
            $query->bindValue(':description', $description[$i], PDO::PARAM_STR);
            $query->bindValue(':price', $price[$i], PDO::PARAM_STR);

            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się zaktualizować menu foodtrucka", 400, $e);
        }
    }
}