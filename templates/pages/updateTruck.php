<?php $truck = $params['foodTruck']; ?>
<?php $products = $params['products']; ?>
<form action="/?action=updateTruck&id=<?php echo $truck[0]['id']; ?>" method="post" enctype="multipart/form-data">
    <div>
        <label for="nameOfImage"></label>
        <input type="text" name="nameOfImage" value="<?php echo $truck[0]['nameImage']; ?>"/>
    </div>
    <div>
        <label for="foodTruckImg"></label>
        <input type="file" name="foodTruckImg"
               value="<?php echo '<img src="data:image/jpeg;base64,' . base64_encode($truck[0]['image']) . '"/>'; ?>"/>
    </div>
    <div>
        <label for="nameFoodTruck"></label>
        <input type="text" name="nameFoodTruck" value="<?php echo $truck[0]['name']; ?>"/>
    </div>
    <div>
        <label for="descriptionTruck"></label>
        <textarea id="description" name="descriptionTruck"><?php echo $truck[0]['description']; ?> </textarea>
    </div>
    <h3 style="text-align:left;">Menu</h3>
    <?php foreach ($products

    as $product): ?>
    <div>
        <label for="nameProduct"></label>
        <input type="text" name="nameOfProducts[]" value="<?php echo $product['product']; ?>"/>
        <label for="prize"></label>
        <input type="text" name="prices[]" value="<?php echo $product['price']; ?>"/>
        <label for="description"></label>
        <div><textarea id="description" name="description[]" rows="3"
                       cols="20"><?php echo $product['description']; ?></textarea>
            <div>
            </div>
            <?php endforeach; ?>
            <div id="howManyProducts">
                <input type="text" id="howMany" name="howMany" placeholder="ile produktów ?"/>
                <input type="button" id="btSubmit" value="submit" onclick="howManyProducts('howMany');">
            </div>
            <input type="submit" name="submit" value="Zatwierdź"/>
</form>