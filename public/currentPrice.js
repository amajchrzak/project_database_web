var XMLHttpRequestObject = false;

if (window.XMLHttpRequest) {
    XMLHttpRequestObject = new XMLHttpRequest();
} else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
}

function countPrice(dest, amountProducts) {
    if (XMLHttpRequestObject) {
        var amountOfProduct = [];
        var pricesOfProduct = [];
        var price = document.getElementById(dest);
        for (var i = 0; i < amountProducts; i++) {
            amountOfProduct[i] = document.getElementById('amount' + i).value;
        }

        for (var i = 0; i < amountProducts; i++) {
            pricesOfProduct[i] = document.getElementById('price' + i).value;
        }
        var url = "helpers/price.php?amount=" + amountOfProduct + "&price=" + pricesOfProduct;
        XMLHttpRequestObject.open("GET", url);
        XMLHttpRequestObject.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                price.innerHTML = this.responseText;
            }
        }
    }
    XMLHttpRequestObject.send(null);
}