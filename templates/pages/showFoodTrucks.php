<?php
//lambda print user data in table
$printUser = function (?string $dataUser) {
    echo isset($dataUser) ? $dataUser : "Nie Ustawiono.";
}
?>

<div class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nazwa</th>
            <th>Nazwa Zdjęcia</th>
            <th>Typ</th>
            <th>Opis</th>
            <th>Opcje</th>
        </tr>
        </thead>
    </table>
</div>
<div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
        <tbody>
        <?php $trucks = $params['trucks']; ?>
        <?php foreach ($trucks as $truck): ?>
            <tr>
                <td><?php $printUser($truck['id']); ?></td>
                <td><?php $printUser($truck['name']); ?></td>
                <td><?php $printUser($truck['nameImage']); ?></td>
                <td><?php $printUser($truck['typeImage']); ?></td>
                <td><?php echo $truck['description'] = substr($truck['description'], 0, 20) . "...";
                    echo $truck['description'] ?></td>
                <td>
                    <a href="/?action=showFoodTruck&id=<?php echo $truck['id']; ?>">
                        <button class="eventButton" type="submit" value="click">Szczegóły</button>
                    </a>
                    <a href="/?action=deleteFoodTruck&id=<?php echo $truck['id']; ?>">
                        <button class="eventButton" type="submit" value="click">USUN</button>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
