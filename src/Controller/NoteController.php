<?php
declare(strict_types=1);

namespace App\Controller;

class NoteController extends AbstractController
{
    //--------------------NOTE------------------

    //Note, user can add commentary
    public function createNoteAction(): void
    {
        $eventId = $this->request->getParam('id');
        if ($this->request->hasPost()) {
            $data['id_user'] = $_SESSION['id_user'];
            $data['login'] = $_SESSION['name'];
            $data['title'] = $this->request->postParam('title');
            $data['description'] = $this->request->postParam('description');
            $data['id_event'] = (int)$this->request->getParam('id');
            if ($this->validNote($data)) {
                $this->noteModel->createNote($data);
                header("Location: /?action=showEvent&id=$eventId");
            } else {
                echo "<div class='errorRegister'>Nie udało się dodać komentarza</div>";
                header("Location: /?action=showEvent&id=$eventId");
            }
        }
    }

    //Note, prints specificaly note

    private function validNote(array $data): bool
    {
        $valid = true;
        if (strlen($data['title']) > 20 || strlen($data['title']) <= 0 ||
            strlen($data['description']) > 500 || strlen($data['description']) <= 0) {
            $valid = false;
            return $valid;
        }
        return $valid;
    }

    public function showNoteAction(): void
    {
        $noteId = (int)$this->request->getParam('id_note');
        $this->view->render('showNote', [
            'note' => $this->noteModel->getNote($noteId)
        ]);
    }
}


?>