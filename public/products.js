var XMLHttpRequestObject = false;

if (window.XMLHttpRequest) {
    XMLHttpRequestObject = new XMLHttpRequest();
} else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
}

function howManyProducts(dest) {
    //jesli obiekt istnieje
    if (XMLHttpRequestObject) {
        var newForms = document.getElementById("howManyProducts");
        var digitOfProducts = document.getElementById(dest).value
        var url = "helpers/howManyProducts.php?how=" + digitOfProducts;
        XMLHttpRequestObject.open("GET", url);
        XMLHttpRequestObject.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                newForms.innerHTML = this.responseText;
                document.getElementById('howMany').disabled = true;
                document.getElementById('btSubmit').disabled = true;
            } else {
                newForms.innerHTML = 'Trwa ładowanie danych - proszę czekać.';
            }
        }
        XMLHttpRequestObject.send(null);
    }
}