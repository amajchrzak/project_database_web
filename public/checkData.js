var XMLHttpRequestObject = false;

if (window.XMLHttpRequest) {
    XMLHttpRequestObject = new XMLHttpRequest();
} else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
}

function checkData(err, name, data) {
    var formData = document.getElementById(name).value;
    var divErr = document.getElementById(err);
    if (XMLHttpRequestObject) {
        var url = "helpers/checkData.php?formData=" + formData + "&data=" + data;
        XMLHttpRequestObject.open("GET", url);
        XMLHttpRequestObject.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                divErr.innerHTML = this.responseText;
            }
        }
    }
    XMLHttpRequestObject.send(null);
}