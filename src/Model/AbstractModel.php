<?php

declare(strict_types=1);

namespace App\Model;

use App\Exception\StorageException;
use PDO;
use PDOException;
use Throwable;

abstract class AbstractModel
{
    protected PDO $conn;

    public function __construct(array $config)
    {
        try {
            if (empty($config['database']) || empty($config['host'])
                || empty($config['user']) || empty($config['password'])) {
                echo "cos nie tak";
            } else {
                $this->createConnection($config);
            }
        } catch (PDOException $e) {
            throw new StorageException('Connection error');
        }
    }

    //Abstract, if $id exist getUserById, if not getUsers, 
    //AdminModel and UserModel using this function so method is in abstract

    private function createConnection($config): void
    {
        $dsn = "mysql:dbname={$config['database']};host={$config['host']}";
        $this->conn = new PDO(
            $dsn,
            $config['user'],
            $config['password'],
            [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
        );
    }

    public function getUsers(?int $id): array
    {
        if (isset($id)) {
            try {
                $query = $this->conn->prepare("
                    SELECT id_user, login, password, e_mail, nazwisko, git, wiek, linkedIn, user_rank
                    FROM users
                    WHERE id_user = :id
                ");
                $query->bindParam(':id', $id, PDO::PARAM_INT);
            } catch (Throwable $e) {
                throw new StorageException('Nie udało się pobrać informacji na temat użytkowników', 400, $e);
            }
        } else {
            try {
                $query = $this->conn->prepare("
                    SELECT * 
                    FROM users
                ");
            } catch (Throwable $e) {
                throw new StorageException('Nie udało się pobrać informacji na temat użytkowników', 400, $e);
            }
        }
        try {
            if (isset($query)) {
                $query->execute();
                $users = $query->fetchAll(PDO::FETCH_ASSOC);
                if ($users === false) {
                    throw new StorageException('Nie udało się pobrać informacji na temat użytkowników', 400);
                }
                $query->closeCursor();
                return $users;
            }
        } catch (Throwable $e) {
            throw new StorageException('Nie udało się pobrać informacji na temat użytkowników', 400, $e);
        }
    }
}