<?php

declare(strict_types=1);

namespace App\Model;

use App\Exception\StorageException;
use PDO;
use Throwable;

class UserModel extends AbstractModel
{
    //User, returns whole notes wrote by loged in user
    public function getUserNotes(): array
    {
        try {
            $id = $_SESSION['id_user'];

            $query = $this->conn->prepare("
                SELECT notes.id_event, users.login, notes.id_note, notes.title, notes.description, notes.date
                FROM notes, users, events
                WHERE notes.id_user = :id
                AND users.id_user = notes.id_user
                AND events.id_event = notes.id_event
                LIMIT 2
            ");
            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();

            $userNotes = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($userNotes === false) {
                throw new StorageException('Lista notatek jest równa 0', 400);
            }
            return $userNotes;
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się pobrać notatek należących do użytkownika użytkownika", 400, $e);
        }
    }

    //User, adding user to database
    public function addUser(array $data): void
    {
        try {
            $login = $data['name'];
            $password = $data['password'];
            $email = $data['email'];

            $query = $this->conn->prepare("
                INSERT INTO users(login, password, e_mail)
                VALUES(?,?,?)
            ");

            $query->bindValue(1, $login, PDO::PARAM_STR);
            $query->bindValue(2, $password, PDO::PARAM_STR);
            $query->bindValue(3, $email, PDO::PARAM_STR);

            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się dodać użytkownika", 400, $e);
        }
    }

    //User, updates data in user profile
    public function updateUser(): void
    {
        try {
            $id = $_SESSION['id_user'];

            $surname = $_SESSION['surname'];
            $age = $_SESSION['age'];
            $git = $_SESSION['git'];
            $linkedIn = $_SESSION['linkedIn'];

            $query = $this->conn->prepare("
                UPDATE users
                SET nazwisko = :surname, wiek = :age, git = :git, linkedIn = :linkedIn
                WHERE id_user = :id
            ");

            $query->bindValue(':surname', $surname, PDO::PARAM_STR);
            $query->bindValue(':age', $age, PDO::PARAM_INT);
            $query->bindValue(':git', $git, PDO::PARAM_STR);
            $query->bindValue(':linkedIn', $linkedIn, PDO::PARAM_STR);
            $query->bindValue(':id', $id, PDO::PARAM_INT);

            $query->execute();
        } catch (Throwable $e) {
            throw new StorageException("Nie udało się zaktualizować profilu", 400, $e);
        }
    }

}