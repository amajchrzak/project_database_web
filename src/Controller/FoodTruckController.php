<?php

declare(strict_types=1);

namespace App\Controller;

class FoodTruckController extends AbstractController
{
    //FOODTRUCKS
    public function addFoodTruckAction(): void
    {
        if ($this->request->hasPost()) {
            $valid = true;
            $_FILES['foodTruckImg']['name'] = $this->request->postParam('nameOfImage');
            $file['nameFoodTruck'] = $this->request->postParam('nameFoodTruck');
            $file['descriptionTruck'] = $this->request->postParam('descriptionTruck');
            $valid = $this->validAddingTruck($file, $_FILES['foodTruckImg']['name'], $_FILES['foodTruckImg'], $valid);
            if ($valid) {
                $this->foodTruckModel->addFoodTruck($_FILES['foodTruckImg'], $file);
                $lastTruck['id'] = $this->foodTruckModel->selectLastTruck();
                $products = $this->request->postParam('nameOfProducts'); //$_POST['nameOfProducts'];
                $prices = $this->request->postParam('prices'); //$_POST['prices'];
                $description = $this->request->postParam('description');
                for ($i = 0; $i < count($products); $i++) {
                    $this->foodTruckModel->addProductList($lastTruck['id'], $products[$i], $prices[$i],
                        $description[$i]);
                }
                header('Location: /');
            }
        }
        $this->view->render('addFoodTruck', []);
    }

    private function validAddingTruck(array $file, string $nameOfImage, array $image, bool $valid): bool
    {
        //valid name of truck
        if (strlen($file['nameFoodTruck']) > 20) {
            echo "<div class='errorRegister'>Nazwa Food Trucka jest za długa</div>";
            $valid = false;
        } else {
            if (strlen($file['nameFoodTruck']) <= 0) {
                echo "<div class='errorRegister'>Nazwa Food Trucka jest za krótka</div>";
                $valid = false;
            }
        }
        if (strlen($file['descriptionTruck']) > 300) {
            echo "<div class='errorRegister'>Opis food trucka jest za długi</div>";
            $valid = false;
        } else {
            if (strlen($file['descriptionTruck']) <= 20) {
                echo "<div class='errorRegister'>Opis food trucka jest za krótki</div>";
                $valid = false;
            }
        }

        //valid name of image
        if (strlen($nameOfImage) > 20) {
            echo "<div class='errorRegister'>Nazwa zdjęcia jest za długa</div>";
            $valid = false;
        } else {
            if (strlen($nameOfImage) <= 0) {
                echo "<div class='errorRegister'>Nazwa zdjęcia jest za krótka</div>";
                $valid = false;
            }
        }
        if ($image['type'] == null) {
            echo "<div class='errorRegister'>Nie wstawiłeś zdjęcia</div>";
            $valid = false;
        }
        return $valid;
    }

    public function showFoodTruckAction(): void
    {
        $idTruck = (int)$this->request->getParam('id');
        $this->view->render('showFoodTruck', [
            'foodTruck' => $this->foodTruckModel->showFoodTrucks($idTruck),
            'products' => $this->foodTruckModel->showProductList($idTruck)
        ]);
    }

    public function updateTruckAction(): void
    {
        $idTruck = (int)$this->request->getParam('id');
        if ($this->request->hasPost()) {
            $_FILES['foodTruckImg']['name'] = $this->request->postParam('nameOfImage');
            $data['nameFoodTruck'] = $this->request->postParam("nameFoodTruck");
            $data['descriptionTruck'] = $this->request->postParam("descriptionTruck");
            $this->foodTruckModel->updateTruck($_FILES['foodTruckImg'], $data, $idTruck);
            $lastTruck['id'] = $this->foodTruckModel->selectLastTruck();
            if (isset($products)) {
                for ($i = 0; $i < 4; $i++) {
                    $products = $this->request->postParam('nameOfProducts');
                    $prices = $this->request->postParam('prices');
                    $description = $this->request->postParam('description');
                    $this->foodTruckModel->updateProductList($products, $prices, $description, $idTruck, $i);
                }
            }
            header('Location: /');
        }
        $this->view->render('updateTruck', [
            'foodTruck' => $this->foodTruckModel->showFoodTrucks($idTruck),
            'products' => $this->foodTruckModel->showProductList($idTruck)
        ]);
    }
}