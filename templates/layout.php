<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>Food Truck</title>
    <link rel="stylesheet" href="/public/style.css" type="text/css"/>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@1,400;1,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/public/fontello/css/fontello.css" type="text/css"/>
</head>
<body onload="getImgDiv(0,'gallery');">
<div class="title">
    <h1 style="opacity:1;">Food Truck Festiwals !</h1>
</div>
<div class="container">
    <div class="menu">
        <ul>
            <li><a href="/">Strona główna</a></li>
            <?php if (!isset($_SESSION['id_user'])) : ?>
                <li><a href="/?action=login">Zaloguj się</a></li>
                <li><a href="/?action=signUp">Zarejestruj się</a></li>
            <?php else: ?>

                <li><a href="/?action=showProfile">Profil</a></li>
                <li><a href="/?action=logout">Wyloguj się</a></li>
                <?php if ($_SESSION['user_rank'] == 1): ?>
                    <li id="users"><a href="/?action=showUsersList">Użytkownicy</a></li>
                    <li id="users"><a href="/?action=getFoodTrucksList">Food Trucki</a></li>
                <?php endif; ?>
            <?php endif; ?>


        </ul>
    </div>
    <div class="page">
        <?php
        require("templates/pages/$page.php");
        ?>
    </div>

    <div class="footer">
        <div class="information">
            <p>Strona o tematyce jedzenia food-truck fast-food, Zapraszam na social media</p>
        </div>
        <div class="socialmedia">
            <a href="facebook.com" class="linkstyle">
                <div class="fb"><i class="icon-facebook-squared"></i></div>
            </a>
            <a href="instagram.com" class="linkstyle">
                <div class="ig"><i class="icon-instagram"></i></div>
            </a>
            <a href="twitter.com" class="linkstyle">
                <div class="tw"><i class="icon-twitter-squared"></i></div>
            </a>
        </div>
    </div>
</div>
<script src="public/validation.js"></script>
<script src="public/products.js"></script>
<script src="public/comentaries.js"></script>
<script src="public/gallery.js"></script>
<script src="public/currentPrice.js"></script>
<script src="public/checkData.js"></script>
</body>
</html>