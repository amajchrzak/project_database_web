<?php $user = $params['userData']; ?>
<?php $fileDir = "resources/profile_img/".$_SESSION['id_user'].".jpg"?>
<!-- jesli zdjecie profilowe istnieje to je ustaw, jesli nie ustaw blanka -->
<?php if(file_exists($fileDir)):?>
        <div id="profileImg">
            <img class="profilePhoto" src="<?php echo $fileDir; ?>" alt="zdjecie profilowe"/>
        </div>
<?php else: ?>
    <img id="blank_profile" src="resources/blank_profile.jpg" style="width:240px;"/>
<?php endif;?>
<!-- formularz do edycji uzytkownika -->
<form action="/?action=edit" method="post" enctype="multipart/form-data">
    <div class="form-edit">
        <span>Zdjęcie profilowe:</span>
        <input type="file" name="profileImg" value=""/>
        <input type="text" id="name" name="name" value="<?php echo $user[0]['login']?>" placeholder="Imię"/><div id="iname" class="formError"></div>
        <input type="text" id="surname" name="surname" value="<?php echo $user[0]['nazwisko']?>" onkeyup="valid('surname', 20)" placeholder="Nazwisko"/><div id="isurname" class="formError"></div>
        <input type="text" id="age" name="age" value="<?php echo $user[0]['wiek']?>" onkeyup="valid('age', 3)" placeholder="Wiek"/><div id="iage" class="formError"></div>
        <input type="text" id="git" name="git" value="<?php echo $user[0]['git']?>" onkeyup="valid('git', 20)" placeholder="Git"/><div id="igit" class="formError"></div>
        <input type="text" id="linkedIn" name="linkedIn" value="<?php echo $user[0]['linkedIn']?>" onkeyup="valid('linkedIn', 20)" placeholder="LinkedIn"/><div id="ilinkedIn" class="formError"></div>
        <input type="submit" value="Zapisz" />
    </div>
</form>


<?php 
    // if(isset($_SESSION['surname'])){
    //     echo $_SESSION['surname'];
    // }
?>