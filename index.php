<?php

declare(strict_types=1);

require_once("src/Utils/debug.php");

spl_autoload_register(function (string $name) {
    $name = str_replace("App\\", "", $name);
    $name = str_replace("\\", "/", $name);
    $name = "src/" . $name . ".php";
    require_once($name);
});

use App\Controller\AbstractController;
use App\Controller\ViewerController;
use App\Request;

session_start();

$configuration = config();
$request = new Request($_GET, $_POST, $_SERVER);
AbstractController::initConfiguration($configuration);

(new ViewerController($request))->run();


function config(): array
{
    return [
        'db' => [
            'host' => 'localhost',
            'database' => 'foodtruck',
            'user' => 'foodtruck_u',
            'password' => 'foodtruckhaslo'
        ]
    ];
}