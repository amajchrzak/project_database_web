<?php

declare(strict_types=1);

namespace App\Controller;

class ViewerController extends AbstractController
{
    //User, function to register the user which has no accout
    public function signUpAction(): void
    {
        $page = 'signUp';
        if ($this->request->hasPost()) {
            $data ['id_user'] = $this->request->postParam('id_user');
            $data ['name'] = $this->request->postParam('name');
            $data ['password'] = $this->request->postParam('password');
            $data ['repassword'] = $this->request->postParam('repassword');
            $data ['email'] = $this->request->postParam('email');
        }
        if (!empty($data)) {
            $this->validateSign($data) ? $page = 'login' : $page = 'signUp';
        }
        $this->view->render($page, [
            $this->userModel->getUsers(null)
        ]);
    }

    //User, function to login 

    private function validateSign(array $data): bool
    {
        $users = $this->userModel->getUsers(null);
        $signUp = false;
        if (($data['name'] === "") || (strlen($data['name']) > 20) ||
            ($data['password'] === "") || (strlen($data['password']) > 20) ||
            ($data['repassword'] !== $data['password'])) {
            echo "<div class='errorRegister'>Wprowadzono błędne dane do rejestracji.</div>";
        } else {
            $emailValidation = filter_var($data['email'], FILTER_SANITIZE_EMAIL);
            if ((filter_var($emailValidation,
                        FILTER_VALIDATE_EMAIL) == false) || ($emailValidation != $data['email'])) {
                echo "Podałeś niepoprawny adres e-mail";
            } else {
                foreach ($users as $user) {
                    if ($data['name'] == $user['login']) {
                        echo "<div class='errorRegister'>Podany login jest zajęty.</div>";
                        return $signUp;
                    }
                    if ($data['email'] == $user['e_mail']) {
                        echo "<div class='errorRegister'>Podany email jest zajęty.</div>";
                        return $signUp;
                    }
                }
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
                $this->userModel->addUser($data);
                $signUp = true;
            }
        }
        return $signUp;
    }

    //User, create session for the user PRIVATE

    public function loginAction(): void
    {
        $page = 'login';
        $login = false; //do walidacji
        if ($this->request->hasPost()) {
            $data ['name'] = $this->request->postParam('name');
            $data ['password'] = $this->request->postParam('password');
        }
        if (!empty($data)) {
            if (($data['name'] === "") || ($data['password'] === "")) {
                $login = false;
            }
            $users = $this->userModel->getUsers(null);
            for ($i = 0; $i < count($users); $i++) {
                if ($data['name'] === $users[$i]['login'] && password_verify($data['password'],
                        $users[$i]['password'])) {
                    $login = true;
                    $this->createUserSession($users[$i]);
                }
            }
            $viewParams['login'] = $login;
        } else {
            $viewParams['login'] = $login;
        }
        $this->view->render($page, $viewParams);
    }

    private function createUserSession(array $users): void
    {
        //session_start();
        $_SESSION['id_user'] = $users['id_user'];
        $_SESSION['name'] = $users['login'];
        $_SESSION['password'] = $users['password'];
        $_SESSION['e_mail'] = $users['e_mail'];
        $_SESSION['user_rank'] = $users['user_rank'];
    }
}