<?php
    //lambda print user data in table
    $printUser = function(?string $dataUser) {echo isset($dataUser) ? $dataUser : "Nie Ustawiono.";}
?>

<div class="tbl-header">
        <table cellpadding="0" cellspacing="0" border="0">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nazwa</th>
                    <th>E-Mail</th>
                    <th>Wiek</th>
                    <th>Git</th>
                    <th>LinkedIn</th>
                    <th>Ranga</th>
                    <th>Opcje</th>
                </tr>
            </thead>
        </table>
</div>
<div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
        <tbody>
            <?php $users = $params['users'];?>
            <?php foreach($users as $user): ?>
                <tr>
                    <td><?php $printUser($user['id_user']); ?></td>
                    <td><?php $printUser($user['login']);?></td>
                    <td><?php $printUser($user['e_mail']);?></td>
                    <td><?php $printUser($user['wiek']);?></td>
                    <td><?php $printUser($user['git']);?></td>
                    <td><?php $printUser($user['linkedIn']);?></td>
                    <td><?php $printUser($user['user_rank']);?></td>
                    <td><a href="/?action=showUser&id_user=<?php echo $user['id_user'];?>"><button class="eventButton" type="submit" value="click">Szczegóły</button></a></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
