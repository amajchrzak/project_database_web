<?php
if (isset($_SESSION['id_user'])):
    ?>
    <h3>Zostałeś pomyślnie zalogowany</h3>
<?php else: ?>
    <h3>Zaloguj się</h3>
    <form action="/?action=login" method="post">
        <div class="login">
            <input type="text" id="name" name="name" placeholder="login" onkeyup="valid('name', 20)"/>
            <div id="iname" class="formError"></div>
            <input type="password" id="password" name="password" placeholder="haslo" onkeyup="valid('password', 30)"/>
            <div id="ipassword" class="formError"></div>
            <input type="submit" value="Zaloguj się"/>
        </div>
    </form>
<?php endif; ?>

