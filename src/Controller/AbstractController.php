<?php

declare(strict_types=1);

namespace App\Controller;

use App\Model\AdminModel;
use App\Model\FoodTruckModel;
use App\Model\NoteModel;
use App\Model\UserModel;
use App\Request;
use App\View;

abstract class AbstractController
{
    protected const DEFAULT_ACTION = 'main';
    protected static array $configuration = [];


    protected NoteModel $noteModel;
    protected UserModel $userModel;
    protected AdminModel $adminModel;
    protected FoodTruckModel $foodTruckModel;

    protected Request $request;
    protected View $view;

    //inicjuje połączenie z bazą danych

    public function __construct(Request $request)
    {
        $this->adminModel = new AdminModel(self::$configuration['db']);
        $this->noteModel = new NoteModel(self::$configuration['db']);
        $this->userModel = new UserModel(self::$configuration['db']);
        $this->foodTruckModel = new FoodTruckModel(self::$configuration['db']);

        $this->request = $request;
        $this->view = new View();
    }

    //konstruktor obiektu odpowiedzialny za akcje

    public function run(): void
    {
        !(isset($_SESSION['user_rank'])) ? $_SESSION['user_rank'] = null : "";

        $action = $this->action() . 'Action';
        if (!method_exists($this, $action)) {
            $configuration = config();
            $request = new Request($_GET, $_POST, $_SERVER);
            AbstractController::initConfiguration($configuration);
            if (method_exists(NoteController::class, $action)) {
                if ($_SESSION['user_rank'] != null) {
                    (new NoteController($request))->run();
                } else {
                    header("Location: /");
                }
            } else {
                if (method_exists(AdminController::class, $action)) {
                    if ($_SESSION['user_rank'] == 1) {
                        (new AdminController($request))->run();
                    } else {
                        header("Location: /");
                    }
                } else {
                    if (method_exists(UserController::class, $action)) {
                        if ($_SESSION['user_rank'] != null) {
                            (new UserController($request))->run();
                        } else {
                            header("Location: /");
                        }
                    } else {
                        if (method_exists(EventController::class, $action)) {
                            if ($_SESSION['user_rank'] != null) {
                                (new EventController($request))->run();
                            } else {
                                header("Location: /");
                            }
                        } else {
                            if (method_exists(FoodTruckController::class, $action)) {
                                if ($_SESSION['user_rank'] != null) {
                                    (new FoodTruckController($request))->run();
                                } else {
                                    header("Location: /");
                                }
                            } else {
                                $action = self::DEFAULT_ACTION . 'Action';
                            }
                        }
                    }
                }
            }
        } else {
            $this->$action();
        }
    }

    //metoda odpowiedzialna za korzystanie użytkownika ze strony

    private function action(): string
    {
        //pobieram parametr z formularza i w zależności od niego dzieje się wybrana akcja dla użytkownika
        return $this->request->getParam('action', self::DEFAULT_ACTION);
    }

    //akcja główna

    public static function initConfiguration(array $configuration): void
    {
        self::$configuration = $configuration;
    }

    public function mainAction(): void
    {
        $this->view->render('main', [
            'events' => $this->adminModel->getEventsData(null),
            'images' => $this->foodTruckModel->showFoodTrucks(null)
        ]);
    }

}