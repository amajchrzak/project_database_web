<div>
    <h3>REJESTRACJA</h3>
    <form action="/?action=signUp" method="post">
        <div class="login">
            <input type="text" id="name" name="name" placeholder="login" onkeyup="checkData('iname', 'name', 0)"/>
            <div id="iname" class="formError"></div>
            <input type="text" id="email" name="email" placeholder="email" onkeyup="checkData('iemail', 'email', 1)"/>
            <div id="iemail" class="formError"></div>
            <input type="password" id="password" name="password" placeholder="hasło" onkeyup="valid('password', 30)"/>
            <div id="ipassword" class="formError"></div>
            <input type="password" id="repassword" name="repassword" placeholder="powtórz hasło"
                   onkeyup="valid('password', 30)"/>
            <div id="irepassword" class="formError"></div>
            <input type="submit" value="Zarejestruj się!">
        </div>
    </form>
</div>